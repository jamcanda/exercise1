import datetime

class Car:

    def __init__(self, cyear, make, model):
        self.cyear = cyear
        self.make = make
        self.model = model

    def age(self):
      today = datetime.date.today()
      age = today.year - self.cyear.year
      return age
    
c1 = Car(datetime.date(1990, 9, 9), "Ford", "Ranger")

print(c1.cyear.year)
print(c1.make)
print(c1.model)
print(c1.age())
